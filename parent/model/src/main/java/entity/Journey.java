package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import entity.base.BaseModel;

@Entity
@Table(name = "JOURNEY")
public class Journey extends BaseModel {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "ORIGINSTATIONID", referencedColumnName = "ID", nullable = false)
	private Station originStation;

	@ManyToOne
	@JoinColumn(name = "DESTINATIONSTATION", referencedColumnName = "ID", nullable = false)
	private Station destinationStation;

	@Temporal(TemporalType.DATE)
	@Column(name = "AVALIABLESTARTDATE", nullable = false)
	private Date avaliableStartDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "AVALIABLEENDDATE")
	private Date avaliableEndDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Station getOriginStation() {
		return originStation;
	}

	public void setOriginStation(Station originStation) {
		this.originStation = originStation;
	}

	public Station getDestinationStation() {
		return destinationStation;
	}

	public void setDestinationStation(Station destinationStation) {
		this.destinationStation = destinationStation;
	}

	public Date getAvaliableStartDate() {
		return avaliableStartDate;
	}

	public void setAvaliableStartDate(Date avaliableStartDate) {
		this.avaliableStartDate = avaliableStartDate;
	}

	public Date getAvaliableEndDate() {
		return avaliableEndDate;
	}

	public void setAvaliableEndDate(Date avaliableEndDate) {
		this.avaliableEndDate = avaliableEndDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Journey other = (Journey) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
