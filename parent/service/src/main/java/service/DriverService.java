package service;

import java.util.List;

import entity.Driver;

public interface DriverService {
	public List<Driver> findAll();

	public Driver findById(Long id);

	public Driver update(Driver driver);

	public void delete(Driver driver);

	public Long save(Driver driver);
}
