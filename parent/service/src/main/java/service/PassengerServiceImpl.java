package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import entity.Passenger;
import intf.PassengerDao;

@Service("passengerService")
@Transactional
public class PassengerServiceImpl implements PassengerService {

	@Autowired
	private PassengerDao dao;

	public List<Passenger> findAll() {
		return dao.findAll();
	}

	public Passenger findById(Long id) {
		return dao.findById(id);
	}

	public Passenger update(Passenger passenger) {
		return dao.update(passenger);
	}

	public void delete(Passenger passenger) {
		dao.delete(passenger);
	}

	public Long save(Passenger passenger) {
		return dao.save(passenger);
	}

}
