package service;

import java.util.List;

import entity.Bus;

public interface BusService {
	public List<Bus> findAll();

	public Bus findById(Long id);

	public Bus update(Bus bus);

	public void delete(Bus bus);

	public Long save(Bus bus);
}
