package service;

import java.util.List;

import entity.Ticket;

public interface TicketService {
	public List<Ticket> findAll();

	public Ticket findById(Long id);

	public Ticket update(Ticket ticket);

	public void delete(Ticket ticket);

	public Long save(Ticket ticket);
}
