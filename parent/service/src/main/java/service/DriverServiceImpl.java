package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import entity.Driver;
import intf.DriverDao;

@Service("driverService")
@Transactional
public class DriverServiceImpl implements DriverService {

	@Autowired
	private DriverDao dao;

	public List<Driver> findAll() {
		return dao.findAll();
	}

	public Driver findById(Long id) {
		return dao.findById(id);
	}

	public Driver update(Driver driver) {
		return dao.update(driver);
	}

	public void delete(Driver driver) {
		dao.delete(driver);
	}

	public Long save(Driver driver) {
		return dao.save(driver);
	}

}
