package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import entity.Journey;
import intf.JourneyDao;

@Service("journeyService")
@Transactional
public class JourneyServiceImpl implements JourneyService {

	@Autowired
	private JourneyDao dao;

	public List<Journey> findAll() {
		return dao.findAll();
	}

	public Journey findById(Long id) {
		return dao.findById(id);
	}

	public Journey update(Journey journey) {
		return dao.update(journey);
	}

	public void delete(Journey journey) {
		dao.delete(journey);
	}

	public Long save(Journey journey) {
		return dao.save(journey);
	}

}
