package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import entity.BusType;
import intf.BusTypeDao;

@Service("busTypeService")
@Transactional
public class BusTypeServiceImpl implements BusTypeService {

	@Autowired
	private BusTypeDao dao;

	public List<BusType> findAll() {
		return dao.findAll();
	}

	public BusType findById(Long id) {
		return dao.findById(id);
	}

	public BusType update(BusType busType) {
		return dao.update(busType);
	}

	public void delete(BusType busType) {
		dao.delete(busType);
	}

	public Long save(BusType busType) {
		return dao.save(busType);
	}

}
