package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import entity.Bus;
import intf.BusDao;

@Service("busService")
@Transactional
public class BusServiceImpl implements BusService {

	@Autowired
	private BusDao busDao;

	public List<Bus> findAll() {
		return busDao.findAll();
	}

	public Bus findById(Long id) {
		return busDao.findById(id);
	}

	public Bus update(Bus bus) {
		return busDao.update(bus);
	}

	public void delete(Bus bus) {
		busDao.delete(bus);
	}

	public Long save(Bus bus) {
		return busDao.save(bus);
	}

}
