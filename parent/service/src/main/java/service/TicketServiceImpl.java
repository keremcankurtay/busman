package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import entity.Ticket;
import intf.TicketDao;

@Service("ticketService")
@Transactional
public class TicketServiceImpl implements TicketService {

	@Autowired
	private TicketDao dao;

	public List<Ticket> findAll() {
		return dao.findAll();
	}

	public Ticket findById(Long id) {
		return dao.findById(id);
	}

	public Ticket update(Ticket ticket) {
		return dao.update(ticket);
	}

	public void delete(Ticket ticket) {
		dao.delete(ticket);
	}

	public Long save(Ticket ticket) {
		return dao.save(ticket);
	}

}
