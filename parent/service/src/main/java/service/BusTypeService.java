package service;

import java.util.List;

import entity.BusType;

public interface BusTypeService {
	public List<BusType> findAll();

	public BusType findById(Long id);

	public BusType update(BusType busType);

	public void delete(BusType busType);

	public Long save(BusType busType);
}
