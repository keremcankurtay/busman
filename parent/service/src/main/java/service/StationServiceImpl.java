package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import entity.Station;
import intf.StationDao;

@Service("stationService")
@Transactional
public class StationServiceImpl implements StationService {

	@Autowired
	private StationDao dao;

	public List<Station> findAll() {
		return dao.findAll();
	}

	public Station findById(Long id) {
		return dao.findById(id);
	}

	public Station update(Station station) {
		return dao.update(station);
	}

	public void delete(Station station) {
		dao.delete(station);
	}

	public Long save(Station station) {
		return dao.save(station);
	}

}
