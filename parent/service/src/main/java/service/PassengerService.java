package service;

import java.util.List;

import entity.Passenger;

public interface PassengerService {
	public List<Passenger> findAll();

	public Passenger findById(Long id);

	public Passenger update(Passenger passenger);

	public void delete(Passenger passenger);

	public Long save(Passenger passenger);
}
