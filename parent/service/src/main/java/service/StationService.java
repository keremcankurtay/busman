package service;

import java.util.List;

import entity.Station;

public interface StationService {
	public List<Station> findAll();

	public Station findById(Long id);

	public Station update(Station station);

	public void delete(Station station);

	public Long save(Station station);
}
