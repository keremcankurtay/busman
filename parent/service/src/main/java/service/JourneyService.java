package service;

import java.util.List;

import entity.Journey;

public interface JourneyService {
	public List<Journey> findAll();

	public Journey findById(Long id);

	public Journey update(Journey journey);

	public void delete(Journey journey);

	public Long save(Journey journey);
}
