package service;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import entity.Bus;
import entity.BusType;
import entity.Driver;
import spring.configuration.ApplicationContextConfiguration;

public class GeneralTestClassMain {

	public static void main(String[] args) {

		AbstractApplicationContext context = new AnnotationConfigApplicationContext(
				ApplicationContextConfiguration.class);

		BusTypeService service = (BusTypeService) context.getBean("busTypeService");

		// BusType busType = new BusType();
		// busType.setName("B�y�k ara�1");
		//
		// service.save(busType);

		List<BusType> busTypeList = service.findAll();

		for (BusType busType2 : busTypeList) {
			System.out.println("BusType.. " + busType2.getName());
		}

		BusType driverFindById = service.findById(1l);

		System.out.println(driverFindById.getName());

		Driver driver = new Driver();
		driver.setName("Kerem Can");
		driver.setSurname("Kurtay");

		Bus bus = new Bus();
		// bus.setBusType(busType);
		bus.setDriver(driver);
		bus.setName("ObssBus1");

		// service.save(bus);

	}

}
