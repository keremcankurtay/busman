package service;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import entity.Bus;
import entity.BusType;
import entity.Driver;
import entity.Journey;
import entity.Station;
import service.configuration.ApplicationContextConfigurationTest;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = ApplicationContextConfigurationTest.class)
public class BusmanTest {

	@Autowired
	private BusTypeService busTypeService;

	@Autowired
	private BusService busService;

	@Autowired
	private DriverService driverService;

	@Autowired
	private StationService stationService;

	@Autowired
	private JourneyService journeyService;

//	@Test
	public void test() {
		BusType busType = testAddBusType();
		Driver driver = testAddDriver();
//		Bus bus = testAddBus(busType, driver);
		// Station originStation = testAddStation("Eseneler Otogar");
		// Station destinationStation = testAddStation("Ankara Otogar");
		// Journey journey = testAddJourney(originStation, destinationStation);

	}

	private BusType testAddBusType() {
		BusType busType = new BusType();
		String busTypeName = "Mercedes";

		busType.setName(busTypeName);

		busTypeService.save(busType);

		BusType bigBusType = busTypeService.findById(1l);

		assertEquals("1 id'li BusType '" + busTypeName + "' olmal�", busTypeName, bigBusType.getName());

		return bigBusType;
	}

	private Driver testAddDriver() {
		Driver driver = new Driver();
		String driverName = "Kerem Can";

		driver.setName(driverName);
		driver.setSurname("Kurtay");

		driverService.save(driver);

		Driver driverKerem = driverService.findById(1l);

		assertEquals("1 id'li Driver '" + driverName + "' olmal�", driverName, driverKerem.getName());

		return driverKerem;

	}

	private Bus testAddBus(BusType busType, Driver driver) {
		Bus bus = new Bus();
		String busName = "Travego";

		bus.setBusType(busType);
		bus.setDriver(driver);
		bus.setName(busName);

		busService.save(bus);

		Bus travego = busService.findById(1l);

		assertEquals("1 id'li Bus '" + busName + "' olmal�", busName, travego.getName());

		return bus;
	}

	private Station testAddStation(String stationName) {
		Station station = new Station();
		station.setName(stationName);

		stationService.save(station);

		Station savedStation = stationService.findById(2l);

		assertEquals("1 id'li Station '" + savedStation + "' olmal�", stationName, savedStation.getName());

		return savedStation;
	}

	private Journey testAddJourney(Station originStation, Station destinationStation) {
		Journey journey = new Journey();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());

		journey.setDestinationStation(destinationStation);
		journey.setOriginStation(originStation);
		journey.setAvaliableStartDate(calendar.getTime());
		calendar.add(Calendar.HOUR_OF_DAY, 6); // adds one hour
		journey.setAvaliableEndDate(calendar.getTime());

		journeyService.save(journey);

		Journey savedJourney = journeyService.findById(1l);

		assertEquals("1 id'li Journey '" + originStation.getName() + "' olmal�", originStation.getName(),
				savedJourney.getOriginStation().getName());

		return savedJourney;
	}
}
