package impl;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import entity.Journey;
import intf.JourneyDao;

@Component
@Repository("journeyDao")
public class JourneyDaoImpl extends BaseDaoImpl<Journey>implements JourneyDao {

	@Override
	void setSession(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

}
