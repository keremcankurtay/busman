package impl;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import entity.base.BaseModel;
import intf.BaseDao;

public abstract class BaseDaoImpl<T extends BaseModel> implements BaseDao<T> {

	@Autowired
	private SessionFactory sessionFactory;

	abstract void setSession(SessionFactory sessionFactory);

	private final Class<T> genericType;

	@SuppressWarnings("unchecked")
	public BaseDaoImpl() {
		this.genericType = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		return getSession().createCriteria(genericType.getName()).list();
	}

	protected Session getSession() {
		return getSessionFactory().getCurrentSession();
	}

	public Long save(T model) {
		return (Long) getSession().save(model);
	}

	@SuppressWarnings("unchecked")
	public T update(T model) {
		return (T) getSession().merge(model);
	}

	public void delete(T model) {
		getSession().delete(model);
	}

	@SuppressWarnings("unchecked")
	public T findById(Long id) {
		return (T) getSession().load(genericType, id);
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
