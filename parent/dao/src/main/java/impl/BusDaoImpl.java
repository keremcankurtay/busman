package impl;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import entity.Bus;
import intf.BusDao;

@Component
@Repository("busDao")
public class BusDaoImpl extends BaseDaoImpl<Bus>implements BusDao {

	public void setSession(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

}
