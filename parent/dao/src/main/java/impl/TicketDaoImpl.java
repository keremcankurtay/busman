package impl;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import entity.Ticket;
import intf.TicketDao;

@Repository
@Component("ticketDao")
public class TicketDaoImpl extends BaseDaoImpl<Ticket>implements TicketDao {

	@Override
	void setSession(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

}
