package impl;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import entity.Driver;
import intf.DriverDao;

@Component
@Repository("driverDao")
public class DriverDaoImpl extends BaseDaoImpl<Driver>implements DriverDao {

	public void setSession(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

}
