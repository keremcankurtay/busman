package impl;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import entity.BusType;
import intf.BusTypeDao;

@Component
@Repository("busTypeDao")
public class BusTypeDaoImpl extends BaseDaoImpl<BusType>implements BusTypeDao {

	public void setSession(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

}
