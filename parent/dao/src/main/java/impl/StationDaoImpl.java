package impl;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import entity.Station;
import intf.StationDao;

@Component
@Repository("stationDao")
public class StationDaoImpl extends BaseDaoImpl<Station>implements StationDao {

	@Override
	void setSession(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

}
