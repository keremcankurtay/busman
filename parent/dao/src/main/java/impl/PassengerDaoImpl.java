package impl;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import entity.Passenger;
import intf.PassengerDao;

@Component
@Repository("passengerDao")
public class PassengerDaoImpl extends BaseDaoImpl<Passenger>implements PassengerDao {

	@Override
	void setSession(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

}
