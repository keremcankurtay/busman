package intf;

import java.util.List;

import entity.base.BaseModel;

public interface BaseDao<T extends BaseModel> {

	public List<T> findAll();

	public Long save(T model);

	public T update(T model);

	public void delete(T model);

	public T findById(Long id);
}
