package intf;

import entity.BusType;

public interface BusTypeDao extends BaseDao<BusType> {
}
