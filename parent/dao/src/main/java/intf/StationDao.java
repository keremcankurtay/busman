package intf;

import entity.Station;

public interface StationDao extends BaseDao<Station> {
}
