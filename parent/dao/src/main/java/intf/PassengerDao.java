package intf;

import entity.Passenger;

public interface PassengerDao extends BaseDao<Passenger> {
}
